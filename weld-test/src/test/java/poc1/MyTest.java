package poc1;

import javax.inject.Inject;

import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
public class MyTest {

	@Inject
	StartupService startupService;

	@Test
	public void testFooPing() {
		Assert.assertEquals("Hello world from poc1.MyServiceImpl", startupService.sayHello());
	}
}
