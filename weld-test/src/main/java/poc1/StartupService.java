package poc1;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class StartupService {

	@Inject
	private MyService myService;

	public String sayHello() {
		return myService.sayHello("world");
	}
}
