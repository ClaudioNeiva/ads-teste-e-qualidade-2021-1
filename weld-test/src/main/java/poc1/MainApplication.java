package poc1;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class MainApplication {
	
	public static void main(String[] args) {
		Weld weld = new Weld();
		try (WeldContainer weldContainer = weld.initialize()) {
			StartupService startupService = weldContainer.select(StartupService.class).get();
			System.out.println(startupService.sayHello());
		}
	}
	
}
