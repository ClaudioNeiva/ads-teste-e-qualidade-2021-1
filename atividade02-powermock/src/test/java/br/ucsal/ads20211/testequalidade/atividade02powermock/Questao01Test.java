package br.ucsal.ads20211.testequalidade.atividade02powermock;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Questao01.class })
public class Questao01Test {

	@Test
	public void testarObterNumeroFaixa1() throws Exception {
		// Entrada: -5, 1500, -34, 9
		// Saída esperada: 9

		Integer nEsperado = 9;

		Scanner scannerMock = Mockito.mock(Scanner.class);
		Mockito.when(scannerMock.nextInt()).thenReturn(-5).thenReturn(1500).thenReturn(-34).thenReturn(9);
		
		PowerMockito.whenNew(Scanner.class).withArguments(System.in).thenReturn(scannerMock);
		
		Integer nAtual = Questao01.obterNumeroFaixa();

		Assert.assertEquals(nEsperado, nAtual);
	}

}
