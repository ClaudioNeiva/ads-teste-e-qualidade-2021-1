package br.ucsal.bes20192.testequalidade.escola.duble;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOMock extends AlunoDAO {

	private Integer qtdChamadasSalvar = 0;

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		return null;
	}

	@Override
	public void excluirTodos() {
	}

	@Override
	public void salvar(Aluno aluno) {
		qtdChamadasSalvar++;
	}

	public void verificarChamadaSalvar(Integer qtdChamadasSalvarEsperada) {
		if (!qtdChamadasSalvarEsperada.equals(qtdChamadasSalvar)) {
			throw new RuntimeException("Qtd chamadas ao salvar foi " + qtdChamadasSalvar + ", mas o esperado era "
					+ qtdChamadasSalvarEsperada);
		}
	}

}
