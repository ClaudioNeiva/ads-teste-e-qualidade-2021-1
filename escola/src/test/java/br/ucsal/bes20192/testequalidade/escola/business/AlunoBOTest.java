package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOTest {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAOMock;
	private DateHelper dateHelperMock;

	@BeforeEach
	public void setup() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateHelperMock = Mockito.mock(DateHelper.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);
	}

	@Test
	void testarIdade21() {
		// Caso de teste
		// Entrada: o ano de referência deve ser 2021 ; anoNascimento = 2000
		// Saída esperada: o aluno 21 anos

		Integer anoReferencia = 2021;

		Integer matricula = 1;
		Integer anoNascimento = 2000;
		Aluno aluno = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(anoNascimento).build();

		Integer idadeEsperada = 21;

		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoReferencia);
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarSalvarAlunoAtivo() {
		// Caso de teste:
		// Entrada: um aluno com situação ATIVO
		// Saída: o aluno ter sido salvo

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().build();

		alunoBO.atualizar(aluno1);

		Mockito.verify(alunoDAOMock).salvar(aluno1);
	}

	@Test
	void testarMatricularAlunoDataNascimentoValida() {
		// Caso de teste:
		// Pré-condição: ano atual seja 2021;
		// Entrada: um aluno com data de nascimento anterior ao ano atual (1980)
		// Saída: o aluno ter sido salvo.

		Integer anoReferencia = 2021;
		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoReferencia);

		alunoBO.matricular("Claudio Neiva", 1980);

		Mockito.verify(alunoDAOMock).salvar(Mockito.any(Aluno.class));
	}

}
