package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.builder.AlunoMother;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAO;
	private DateHelper dateHelper;

	@BeforeEach
	void setup() {
		alunoDAO = new AlunoDAO();
		dateHelper = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateHelper);
		alunoDAO.excluirTodos();
	}

	@Test
	void testarCalculoIdade() {
		// FIXME É necessário ajustar a data do sistema para garantir que o ano atual é
		// 2021.
		
		// Ano atual = 2021
		Integer anoNascimento = 2000;
		Integer idadeEsperada = 21;
		
		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().nascidoEm(anoNascimento).build();
		alunoDAO.salvar(aluno1);
		
		Integer idadeAtual = alunoBO.calcularIdade(aluno1.getMatricula());

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivoUsandoObjectMother() {

		Aluno alunoEsperado = AlunoMother.alunoAtivo();

		alunoBO.atualizar(alunoEsperado);

		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());

		Assertions.assertEquals(alunoEsperado, alunoAtual);

	}

	@Test
	void testarAtualizarAlunoAtivoUsandoTestDataBuilder() {

		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		alunoBO.atualizar(alunoEsperado);

		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());

		Assertions.assertEquals(alunoEsperado, alunoAtual);

	}

	@Test
	void ilustarOTestDataBuilder() {

		Aluno aluno0 = AlunoBuilder.umAluno().comMatricula(20).comNome("Maria").nascidoEm(1950).cancelado().build();

		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(20).comNome("Maria").nascidoEm(1950)
				.cancelado();
		Aluno aluno1 = alunoBuilder.build();
		Aluno aluno2 = alunoBuilder.mas().comMatricula(2323).build();
		Aluno aluno3 = alunoBuilder.mas().comNome("Joaquim").build();
		Aluno aluno4 = alunoBuilder.mas().nascidoEm(1900).build();
		Aluno aluno5 = alunoBuilder.mas().ativo().build();

		System.out.println("aluno0=" + aluno0);
		System.out.println("aluno1=" + aluno1); // 20 Maria 1950 Cancelado
		System.out.println("aluno2=" + aluno2); // 2323 Maria 1950 Cancelado
		System.out.println("aluno3=" + aluno3); // 20 Joaquim 1950 Cancelado
		System.out.println("aluno4=" + aluno4); // 20 Maria 1900 Cancelado
		System.out.println("aluno5=" + aluno5); // 20 Maria 1950 Ativo

	}
}
