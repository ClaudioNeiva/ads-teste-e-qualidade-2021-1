package br.ucsal.bes20192.testequalidade.escola.business.duble;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.duble.AlunoDAOFake;
import br.ucsal.bes20192.testequalidade.escola.duble.DateHelperStub;

public class AlunoBOTest {

	private AlunoBO alunoBO;
	private AlunoDAOFake alunoDAOFake;
	private DateHelperStub dateHelperStub;

	@BeforeEach
	void setup() {
		alunoDAOFake = new AlunoDAOFake();
		dateHelperStub = new DateHelperStub();
		alunoBO = new AlunoBO(alunoDAOFake, dateHelperStub);
	}

	@Test
	void testarCalculoIdade() {
		Integer anoAtual = 2021;
		Integer anoNascimento = 2000;
		Integer idadeEsperada = 21;

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().nascidoEm(anoNascimento).build();
		alunoDAOFake.salvar(aluno1);

		dateHelperStub.definirAnoAtual(anoAtual);

		// calcularIdade é um método QUERY.
		Integer idadeAtual = alunoBO.calcularIdade(aluno1.getMatricula());

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizacaoAlunoAtivo() {

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().build();

		// atualizar é um método COMMAND.
		alunoBO.atualizar(aluno1);

		// Não é possível comprar o retorno de um método command, pois não existe.
		
	}

}
