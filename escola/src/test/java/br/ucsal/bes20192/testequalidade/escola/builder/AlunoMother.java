package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoMother {

	public static Aluno alunoAtivo() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(1);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.ATIVO);
		aluno1.setAnoNascimento(2000);
		return aluno1;
	}

	public static Aluno alunoCancelado() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(1);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.CANCELADO);
		aluno1.setAnoNascimento(2000);
		return aluno1;
	}

	public static Aluno alunoMaiorIdade() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(1);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.ATIVO);
		aluno1.setAnoNascimento(2000);
		return aluno1;
	}

}
