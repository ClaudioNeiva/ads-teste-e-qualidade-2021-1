package br.ucsal.bes20192.testequalidade.escola.business.duble;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.duble.AlunoDAOMock;
import br.ucsal.bes20192.testequalidade.escola.duble.DateHelperStub;

public class AlunoBOUsandoMockTest {

	private AlunoBO alunoBO;
	private AlunoDAOMock alunoDAOMock;
	private DateHelperStub dateHelperStub;

	@BeforeEach
	void setup() {
		alunoDAOMock = new AlunoDAOMock();
		dateHelperStub = new DateHelperStub();
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperStub);
	}

	@Test
	void testarAtualizacaoAlunoAtivo() {

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().build();

		// atualizar é um método COMMAND.
		alunoBO.atualizar(aluno1);

		// Não é possível comprar o retorno de um método command, pois não existe.

		alunoDAOMock.verificarChamadaSalvar(1);
	}

	@Test
	void testarAtualizacaoAlunoCancelado() {

		Aluno aluno1 = AlunoBuilder.umAluno().cancelado().build();

		// atualizar é um método COMMAND.
		alunoBO.atualizar(aluno1);

		// Não é possível comprar o retorno de um método command, pois não existe.

		alunoDAOMock.verificarChamadaSalvar(0);
	}

}
