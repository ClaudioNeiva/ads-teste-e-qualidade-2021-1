package br.ucsal.bes20192.testequalidade.escola.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {

	private static final String USER = "postgres";
	private static final String PASSWORD = "abcd1234";
	private static final String STRING_CONNECTION = "jdbc:postgresql://localhost:5432/escola";

	private Connection connection = null;

	protected Connection getConnection() {
		if (connection == null) {
			conectar();
		}
		return connection;
	}

	private void conectar() {
		try {
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
