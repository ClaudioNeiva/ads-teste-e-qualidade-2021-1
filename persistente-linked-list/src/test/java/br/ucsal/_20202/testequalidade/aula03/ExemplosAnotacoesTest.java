package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ExemplosAnotacoesTest {

	@BeforeAll
	static void setupClass() {
		System.out.println("setupClass");
	}
	
	@BeforeEach
	void setup() {
		System.out.println("	setup");
	}
	
	@AfterEach
	void tearDown() {
		System.out.println("	tearDown");
	}
	
	@AfterAll
	static void tearDownClass() {
		System.out.println("tearDownClass");
	}
	
	@Test
	@DisplayName("Primeiro método de teste")
	void testar1() {
		System.out.println("		testar1");
	}
	
	@Test
	@Disabled
	void testar2() {
		System.out.println("		testar2");
	}
	
	@Test
	void testar3() {
		System.out.println("		testar3");
	}
	
	@Test
	void testar4() {
		System.out.println("		testar4");
	}
	
}
