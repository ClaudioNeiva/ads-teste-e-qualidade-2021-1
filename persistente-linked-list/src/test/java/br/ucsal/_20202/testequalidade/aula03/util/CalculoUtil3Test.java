package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtil3Test {

	@Test
	void testarFatorial() {
		int n = 5;
		long fatorialEsperado = 120L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
