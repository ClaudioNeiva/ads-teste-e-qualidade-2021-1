package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

public class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		// Preparação para o teste
		nomes = new LinkedList<>();
	}

	@Test
	void testarExcecaoAddNull() {
		Throwable exception = Assertions.assertThrows(InvalidElementException.class, () -> nomes.add(null));
		String mensagemEsperada = "The element can't be null.";
		Assertions.assertEquals(mensagemEsperada, exception.getMessage());
	}

	@Test
	void testarAdd1Elemento() throws InvalidElementException {
		// Definição do resultado esperado
		String nomeEsperado = "claudio";

		// Execução do método que eu quero testar
		nomes.add(nomeEsperado);

		// Obtenção do resultado atual
		String nomeAtual = nomes.get(0);

		// COMPARAR O RESULTADO ESPERADO COM O RESULTADO ATUAL
		Assertions.assertEquals(nomeEsperado, nomeAtual);
	}

	@Test
	void testarAdd3Elementos() throws InvalidElementException {
		// Definição do resultado esperado
		String nomeEsperado1 = "claudio";
		String nomeEsperado2 = "antonio";
		String nomeEsperado3 = "neiva";

		// Execução do método que eu quero testar
		nomes.add(nomeEsperado1);
		nomes.add(nomeEsperado2);
		nomes.add(nomeEsperado3);

		// Obtenção do resultado atual
		String nomeAtual1 = nomes.get(0);
		String nomeAtual2 = nomes.get(1);
		String nomeAtual3 = nomes.get(2);

		// COMPARAR O RESULTADO ESPERADO COM O RESULTADO ATUAL
		Assertions.assertAll("Comparando 3 nomes", () -> Assertions.assertEquals(nomeEsperado1, nomeAtual1),
				() -> Assertions.assertEquals(nomeEsperado2, nomeAtual2),
				() -> Assertions.assertEquals(nomeEsperado3, nomeAtual3));

		// Assertions.assertEquals(nomeEsperado1, nomeAtual1);
		// Assertions.assertEquals(nomeEsperado2, nomeAtual2);
		// Assertions.assertEquals(nomeEsperado3, nomeAtual3);

	}

	@Test
	void testarLimpezaLista1Nome() throws InvalidElementException {
		nomes.add("Claudio Neiva");
		int tamanhoEsperado = 0;
		nomes.clear();
		int tamanhoAtual = nomes.size();
		Assertions.assertEquals(tamanhoEsperado, tamanhoAtual);
	}

}
