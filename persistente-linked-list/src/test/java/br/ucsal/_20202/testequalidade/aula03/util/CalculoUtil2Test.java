package br.ucsal._20202.testequalidade.aula03.util;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CalculoUtil2Test {

	@ParameterizedTest(name = "{index} calcularFatorial({0})")
	@MethodSource("fornecerDadosTest")
	void testarFatorial(Integer n, Long fatorialEsperado) {
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	private static Stream<Arguments> fornecerDadosTest() {
		return Stream.of(
				Arguments.of(0, 1L), 
				Arguments.of(1, 1L), 
				Arguments.of(3, 6L), 
				Arguments.of(5, 120L));
	}

}
