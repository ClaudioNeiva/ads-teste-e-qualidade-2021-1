package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public class PersitenteLinkedListTest {

	@Test
	void testarPersist3Nomes() throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		Assumptions.assumeTrue(DbUtil.isConnectionValid());

		Connection connection = DbUtil.getConnection();

		PersistentList<String> nomesEsperados = new PersitenteLinkedList<>();
		nomesEsperados.add("antonio");
		nomesEsperados.add("claudio");
		nomesEsperados.add("neiva");

		nomesEsperados.persist(1L, connection, "lista1");

		PersistentList<String> nomesAtuais = new PersitenteLinkedList<>();
		nomesAtuais.load(1L, connection, "lista1");

		Assertions.assertEquals(nomesEsperados.get(0), nomesAtuais.get(0));
		Assertions.assertEquals(nomesEsperados.get(1), nomesAtuais.get(1));
		Assertions.assertEquals(nomesEsperados.get(2), nomesAtuais.get(2));
	}

}
