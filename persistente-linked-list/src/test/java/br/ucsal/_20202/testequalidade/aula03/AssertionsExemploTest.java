package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class AssertionsExemploTest {

	@Test
	void testar1() {
		assertTrue(Stream.of(10, 8).mapToInt(i -> i).sum() == 18, "A soma deve ser 18.");
	}
	
}
