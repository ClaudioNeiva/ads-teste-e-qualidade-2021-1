package br.ucsal.ads20211.testequalidade.aula13.selenium;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InvestingComFirefoxTest extends InvestingComTest {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.gecko.driver","./drivers/geckodriver-29");
		// System.setProperty("webdriver.firefox.bin", "/usr/bin/firefox");
		driver = new FirefoxDriver();
	}
	
}
