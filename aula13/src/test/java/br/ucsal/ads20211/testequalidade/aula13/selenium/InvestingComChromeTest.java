package br.ucsal.ads20211.testequalidade.aula13.selenium;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvestingComChromeTest extends InvestingComTest {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver-91");
		driver = new ChromeDriver();
	}

}
