package br.ucsal.ads20211.testequalidade.aula13.jbehave;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PesquisaInvestingSteps {

	private WebDriver driver;

	@Given("estou no $browser")
	public void abrirBrowser(String browser) {
		if (browser.equalsIgnoreCase("GoogleChrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver-91");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver-29");
			driver = new FirefoxDriver();
		} else {
			throw new RuntimeException("Browser não configurado.");
		}
	}

	@Given("estou na página $url")
	public void abrirPaginaWeb(String url) {
		driver.get(url);
	}

	@When("informo $conteudo no campo de pesquisa")
	public void preencherCampoPesquisa(String conteudo) {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(conteudo + Keys.ENTER);
	}

	@Then("o nome $conteudoEsperado aparece na tela")
	public void verificarConteudo(String conteudoEsperado) throws InterruptedException {
		Thread.sleep(3000);
		String conteudoAtual = driver.getPageSource();
		Assertions.assertTrue(conteudoAtual.toUpperCase().contains(conteudoEsperado.toUpperCase()));
	}

	@Then("o browser é fechado")
	public void fecharBrowser() {
		driver.close();
	}

}
