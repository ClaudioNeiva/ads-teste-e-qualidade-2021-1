Narrativa:
Como um investidor
desejo pesquisar uma empresa pelo seu código
de modo que possa saber o nome da mesma

Cenário: Pesquisa da empresa BBDC4 
Dado que estou no GoogleChrome
E estou na página http://www.investing.com
Quando informo BBDC4 no campo de pesquisa
Então o nome Bradesco aparece na tela
E o browser é fechado

Cenário: Pesquisa da empresa USIM5 
Dado que estou no Firefox
E estou na página http://www.investing.com
Quando informo USIM5 no campo de pesquisa
Então o nome Usiminas aparece na tela
E o browser é fechado
